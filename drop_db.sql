DROP TABLE accounts;
DROP TABLE books;
DROP TABLE user_roles;
DROP TABLE users;

DROP SEQUENCE seq_accounts;
DROP SEQUENCE seq_books;
DROP SEQUENCE seq_users;
DROP SEQUENCE seq_user_roles;