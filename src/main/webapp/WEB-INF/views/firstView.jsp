<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>

<jsp:include page="layout/header.jsp" />

<body>

  <jsp:include page="layout/navbar.jsp" />

  <div class="container">
  
    <h1 class="text-center">Uživatel</h1>
    
    <div class="table-responsive">
      <table class="table table-hover bordered-table">
   		
   		<tbody>
   		  <tr>
   		    <td><strong>Jméno: </strong></td>
   		    <td><c:out value="${person.name}" /></td>
   		  </tr>
   		  <tr>
   		    <td><strong>Příjmení: </strong></td>
   		    <td><c:out value="${person.surname}" /></td>
   		  </tr>
   		  <tr>
   		    <td><strong>Telefon: </strong></td>
   		    <td><c:out value="${person.phoneNumber}" /></td>
   		  </tr>
   		  <tr>
   		    <td><strong>Datum: </strong></td>
   		    <td><fmt:formatDate pattern="dd.MM.yyyy HH:mm:ss" value="${person.date}" /></td>
   		  </tr>
   		  <tr>
   		    <td><strong>Záliby: </strong></td>
   		    <td>
   		    <c:forEach var="like" items="${person.likes}" varStatus="loop">
   		      <c:out value="${like}" /><c:if test="${!loop.last}"><br /></c:if>
   		    </c:forEach>
   		    </td>
   		  <tr>
   		  <tr>
   		    <td><strong>Přátelé: </strong></td>
   		    <td>
   		    <c:forEach var="friend" items="${person.friends}" varStatus="loop">
   		      <c:out value="${friend.key}" /> - <c:out value="${friend.value}" /><c:if test="${!loop.last}"><br /></c:if>
   		    </c:forEach>
   		    </td>
   		  </tr>
   		</tbody>
      </table>
	</div>
	
  </div>
  
</body>

<jsp:include page="layout/footer.jsp" />

</html>