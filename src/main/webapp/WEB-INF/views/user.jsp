<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<jsp:include page="layout/header.jsp" />

<body>

	<jsp:include page="layout/navbar.jsp" />

	<div class="container">

		<h1 class="text-center">Uživatel</h1>

		<c:if test="${userEdited == false}">
			<div class="alert alert-danger" role="alert">Editace uživatele
				se nezdařila!</div>
		</c:if>

		<c:if test="${userEdited == true}">
			<div class="alert alert-success" role="alert">Uživatel byl
				úspěšně editován!</div>
		</c:if>

		<c:if test="${bookAdded == true}">
			<div class="alert alert-success" role="alert">Nová kniha
				přidána!</div>
		</c:if>

		<c:if test="${bookFound == false}">
			<div class="alert alert-danger" role="alert">Požadovaná kniha
				neexistuje!</div>
		</c:if>

		<c:if test="${bookDeleted == true}">
			<div class="alert alert-success" role="alert">Kniha byla
				úspěšně smazána!</div>
		</c:if>

		<c:if test="${bookDeleted == false}">
			<div class="alert alert-danger" role="alert">Nepodařilo se
				smazat knihu!</div>
		</c:if>

		<c:if test="${bookEdited == true}">
			<div class="alert alert-success" role="alert">Kniha byla
				úspěšně upravena!</div>
		</c:if>

		<c:if test="${bookEdited == false}">
			<div class="alert alert-danger" role="alert">Nepodařilo se
				upravit knihu!</div>
		</c:if>

		<c:if test="${accountAdded == true}">
			<div class="alert alert-success" role="alert">Účet byl přidán!</div>
		</c:if>

		<c:if test="${accountDeleted == true}">
			<div class="alert alert-success" role="alert">Účet byl úspěšně
				smazán!</div>
		</c:if>

		<c:if test="${accountDeleted == false}">
			<div class="alert alert-danger" role="alert">Nepodařilo se
				smazat účet!</div>
		</c:if>

		<c:if test="${accountFound == false}">
			<div class="alert alert-danger" role="alert">Požadovaný účet
				neexistuje!</div>
		</c:if>

		<c:if test="${accountEdited == true}">
			<div class="alert alert-success" role="alert">Účet byl úspěšně
				upraven!</div>
		</c:if>

		<c:if test="${accountEdited == false}">
			<div class="alert alert-danger" role="alert">Nepodařilo se
				upravit účet!</div>
		</c:if>

		<div class="table-responsive">
			<table class="table table-hover user-table">

				<tbody>
					<tr>
						<td><strong>Jméno: </strong></td>
						<td><c:out value="${user.name}" /></td>
					</tr>
				</tbody>
			</table>
		</div>

		<a href="<c:url value='/admin/addBook/${user.id}' />"
			class="btn btn-primary active margin-bottom" role="button">Přidat
			knihu</a>

		<c:choose>
			<c:when test="${empty user.books}">
				<div class="alert alert-info" role="alert">Uživatel nemá žádné
					knihy!</div>
			</c:when>
			<c:otherwise>
				<div class="table-responsive">
					<table class="table table-hover">

						<thead>
							<tr>
								<th>Název</th>
								<th>Popis</th>
								<th>Upravit</th>
								<th>Smazat</th>
							</tr>
						</thead>

						<tbody>
							<c:forEach var="book" items="${user.books}">
								<c:choose>
									<c:when test="${book.isPreferable == true}">
										<tr class="warning">
									</c:when>
									<c:otherwise>
										<tr>
									</c:otherwise>
								</c:choose>
								<td><c:out value="${book.title}" /></td>
								<td><c:out value="${book.description}" /></td>
								<td><a
									href="<c:url value='/admin/editBook/${user.id}/${book.id}' />"
									class="btn btn-success btn-xs">Upravit</a></td>
								<td><a
									href="<c:url value='/admin/deleteBook/${user.id}/${book.id}' />"
									class="btn btn-success btn-xs">Smazat</a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</c:otherwise>
		</c:choose>

		<a href="<c:url value='/admin/addAccount/${user.id}' />"
			class="btn btn-primary active margin-bottom" role="button">Přidat
			účet</a>

		<c:choose>
			<c:when test="${empty user.accounts}">
				<div class="alert alert-info" role="alert">Uživatel nemá žádné
					účty!</div>
			</c:when>
			<c:otherwise>
				<div class="table-responsive">
					<table class="table table-hover">

						<thead>
							<tr>
								<th>Název</th>
								<th>Předčíslí</th>
								<th>Číslo</th>
								<th>Kód banky</th>
								<th>Upravit</th>
								<th>Smazat</th>
							</tr>
						</thead>

						<tbody>
							<c:forEach var="account" items="${user.accounts}">
								<c:choose>
									<c:when test="${account.isPreferable == true}">
										<tr class="warning">
									</c:when>
									<c:otherwise>
										<tr>
									</c:otherwise>
								</c:choose>
								<td><c:out value="${account.name}" /></td>
								<td><c:out value="${account.accountPrefix}" /></td>
								<td><c:out value="${account.accountNumber}" /></td>
								<td><c:out value="${account.bankCode}" /></td>
								<td><a
									href="<c:url value='/admin/editAccount/${user.id}/${account.id}' />"
									class="btn btn-success btn-xs">Upravit</a></td>
								<td><a
									href="<c:url value='/admin/deleteAccount/${user.id}/${account.id}' />"
									class="btn btn-success btn-xs">Smazat</a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</c:otherwise>
		</c:choose>

		<a href="<c:url value='/users' />"
			class="btn btn-warning active margin-bottom" role="button">Zpět
			na seznam</a>

	</div>

</body>

<jsp:include page="layout/footer.jsp" />

</html>