<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>

<jsp:include page="layout/header.jsp" />

<body>

	<jsp:include page="layout/navbar.jsp" />

	<div class="container">

		<h1 class="text-center">Nový uživatel</h1>

		<c:url value='/admin/addUser' var="addUser" />
		<form:form class="form-horizontal" method="post"
			action="${addUser}" modelAttribute="user">

			<div class="form-group">
				<label for="name" class="col-sm-2 control-label"><em>*</em>Jméno</label>
				<div class="col-sm-5">
					<form:input type="text" path="name" class="form-control" id="name"
						placeholder="Jméno" />
				</div>
				<form:errors path="name" cssClass="control-label form-errors" />
			</div>
			
			<div class="form-group">
				<label for="password" class="col-sm-2 control-label"><em>*</em>Heslo</label>
				<div class="col-sm-5">
					<form:input type="password" path="password" class="form-control" id="password"
						placeholder="Heslo" />
				</div>
				<form:errors path="password" cssClass="control-label form-errors" />
			</div>

			<div class="form-group">
				<div class="col-sm-9">
					<button type="submit" class="btn btn-success pull-left">Vytvořit</button>
				</div>
			</div>

		</form:form>

		<a href="<c:url value='/users' />"
			class="btn btn-warning active margin-bottom" role="button">Zpět
			na seznam</a>

	</div>

</body>

<jsp:include page="layout/footer.jsp" />

</html>