<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>

<jsp:include page="layout/header.jsp" />

<body>

	<jsp:include page="layout/navbar.jsp" />

	<div class="container">

		<h1 class="text-center">Nová kniha</h1>

		<c:url value='/admin/addBook/${userId}' var='addBook' />
		<form:form class="form-horizontal" method="post"
			action="${addBook}"
			modelAttribute="book">

			<div class="form-group">
				<label for="title" class="col-sm-2 control-label"><em>*</em>Název</label>
				<div class="col-sm-5">
					<form:input type="text" path="title" class="form-control"
						id="title" placeholder="Název" />
				</div>
				<form:errors path="title" cssClass="control-label form-errors" />
			</div>


			<div class="form-group">
				<label for="description" class="col-sm-2 control-label"><em>*</em>Popis</label>
				<div class="col-sm-9">
					<form:textarea class="form-control" rows="10" id="description"
						path="description" />
				</div>
				<form:errors path="description" cssClass="control-label form-errors" />
			</div>

			<div class="form-group">
				<div class="col-sm-9">
					<button type="submit" class="btn btn-success pull-left">Vytvořit</button>
				</div>
			</div>

		</form:form>

		<a href="<c:url value='/user/${userId}' />"
			class="btn btn-warning active margin-bottom" role="button">Zpět
			na detail uživatele</a>

	</div>

</body>

<jsp:include page="layout/footer.jsp" />

</html>