<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>

<jsp:include page="layout/header.jsp" />

<body>

	<jsp:include page="layout/navbar.jsp" />

	<div class="container">

		<h1 class="text-center">Editace účtu</h1>

		<c:url value='/admin/editAccount/${userId}' var='editAccount' />
		<form:form class="form-horizontal" method="post"
			action="${editAccount}"
			modelAttribute="account">

			<form:hidden path="id" />

			<div class="form-group">
				<label for="name" class="col-sm-2 control-label"><em>*</em>Název</label>
				<div class="col-sm-5">
					<form:input type="text" path="name" class="form-control" id="name" />
				</div>
				<form:errors path="name" cssClass="control-label form-errors" />
			</div>

			<div class="form-group">
				<label for="accountPrefix" class="col-sm-2 control-label"><em>*</em>Předčíslí
					účtu</label>
				<div class="col-sm-5">
					<form:input type="text" path="accountPrefix" class="form-control"
						id="accountPrefix" />
				</div>
				<form:errors path="accountPrefix"
					cssClass="control-label form-errors" />
			</div>

			<div class="form-group">
				<label for="accountNumber" class="col-sm-2 control-label"><em>*</em>Číslo
					účtu</label>
				<div class="col-sm-5">
					<form:input type="text" path="accountNumber" class="form-control"
						id="accountNumber" />
				</div>
				<form:errors path="accountNumber"
					cssClass="control-label form-errors" />
			</div>

			<div class="form-group">
				<label for="bankCode" class="col-sm-2 control-label"><em>*</em>Kód
					banky</label>
				<div class="col-sm-5">
					<form:input type="text" path="bankCode" class="form-control"
						id="bankCode" />
				</div>
				<form:errors path="bankCode" cssClass="control-label form-errors" />
			</div>

			<div class="form-group">
				<div class="col-sm-9">
					<button type="submit" class="btn btn-success pull-left">Upravit</button>
				</div>
			</div>

		</form:form>

		<a href="<c:url value='/user/${userId}' />"
			class="btn btn-warning active margin-bottom" role="button">Zpět
			na detail uživatele</a>

	</div>

</body>

<jsp:include page="layout/footer.jsp" />

</html>