<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>

<jsp:include page="layout/header.jsp" />

<body>

	<jsp:include page="layout/navbar.jsp" />

	<div class="container">

		<h1 class="text-center">Editace uživatele</h1>

		<c:url value='/admin/editUser' var='editUser' />
		<form:form class="form-horizontal" method="post"
			action="${editUser}" modelAttribute="user">

			<form:hidden path="id" />
			
			<div class="form-group">
				<label for="name" class="col-sm-2 control-label"><em>*</em>Jméno</label>
				<div class="col-sm-5">
					<form:input type="text" path="name" class="form-control" id="name" />
				</div>
				<form:errors path="name" cssClass="control-label form-errors" />
			</div>

			<c:if test="${!empty user.books}">
				<div class="form-group">
					<label for="preferableBookId" class="col-sm-2 control-label"><em></em>Preferovaná
						kniha</label>
					<div class="col-sm-5">
						<form:select class="form-control" id="preferableBookId"
							path="preferableBookId">
							<form:option value="0">--- Vyberte knihu ---</form:option>
							<c:forEach var="book" items="${user.books}">
								<form:option value="${book.id}">
									<c:out value="${book.title}"></c:out>
								</form:option>
							</c:forEach>
						</form:select>
					</div>
				</div>
			</c:if>

			<c:if test="${!empty user.accounts}">
				<div class="form-group">
					<label for="preferableAccountId" class="col-sm-2 control-label"><em></em>Preferovaný
						účet</label>
					<div class="col-sm-5">
						<form:select class="form-control" id="preferableAccountId"
							path="preferableAccountId">
							<form:option value="0">--- Vyberte účet ---</form:option>
							<c:forEach var="account" items="${user.accounts}">
								<form:option value="${account.id}">
									<c:out value="${account.name}"></c:out>
								</form:option>
							</c:forEach>
						</form:select>
					</div>
				</div>
			</c:if>

			<div class="form-group">
				<div class="col-sm-9">
					<button type="submit" class="btn btn-success pull-left">Upravit</button>
				</div>
			</div>

		</form:form>

		<a href="<c:url value='/users' />"
			class="btn btn-warning active margin-bottom" role="button">Zpět
			na seznam</a>

	</div>

</body>

<jsp:include page="layout/footer.jsp" />

</html>