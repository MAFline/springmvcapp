<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<jsp:include page="layout/header.jsp" />

<body>

	<jsp:include page="layout/navbar.jsp" />

	<div class="container">

		<h1 class="text-center">Rozcestník</h1>

		<c:if test="${param.error == true}">
			<div class="alert alert-danger" role="alert">Přihlášení se
				nezdařilo!</div>
		</c:if>

		<c:if test="${param.logged == true}">
			<div class="alert alert-success" role="alert">Přihlášení
				proběhlo úspěšně!</div>
		</c:if>

		<c:if test="${param.logout == true}">
			<div class="alert alert-success" role="alert">Byl jste úspěšně
				odhlášen!</div>
		</c:if>

		<div class="table-responsive">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>Fáze</th>
						<th>Úkoly</th>
					</tr>
				</thead>

				<tbody>
					<tr>
						<td rowspan="2" style="vertical-align: middle">První</td>
						<td>Controller s testovacími daty <a
							href="<c:url value='/firstController' />">firstController</a>.
						</td>
					</tr>
					<tr>
						<td>Zobrazování obrázků z adresáře konfigurovatelného ze
							/src/main/resources/config.properties. Testovaný příklad <a
							href="<c:url value='/showPicture?filename=real.png' />">real.png</a>.
						</td>
					</tr>
					<tr>
						<td>Druhá</td>
						<td>Zobrazení uživatele z databáze podle ID. <a
							href="<c:url value='/user/1' />">User 1</a>, <a
							href="<c:url value='/user/2' />">User 2</a> a <a
							href="<c:url value='/user/3' />">User 3</a>.
						</td>
					</tr>
					<tr>
						<td rowspan="2" style="vertical-align: middle">Třetí/Čtvrtá</td>
						<td>Seznam uživatelů. <a href="<c:url value='/users' />">Uživatelé</a>.
						</td>
					</tr>
					<tr>
						<td>Přidání uživatele. <a
							href="<c:url value='/admin/addUser' />">Formulář</a>.
						</td>
					</tr>
				</tbody>
			</table>
		</div>

	</div>

</body>

<jsp:include page="layout/footer.jsp" />

</html>