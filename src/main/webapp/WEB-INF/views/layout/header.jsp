<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>Morosystems Example</title>
  <link href="<c:url value='/resources/css/bootstrap.css' />" rel="stylesheet" />
  <link href="<c:url value='/resources/css/style.css' />" rel="stylesheet" />
</head>