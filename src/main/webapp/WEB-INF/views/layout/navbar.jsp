<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<nav class="navbar navbar-default">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
				aria-expanded="false">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<p class="navbar-text">
				<spring:message code="last.update" /> <fmt:formatDate pattern="dd.MM.yyyy HH:mm:ss" value="${date}" />
			</p>
			<p class="navbar-text">
				<spring:message code="users.in.db" />: 
				<c:out value="${numberOfUsers}" />
			</p>
			<p class="navbar-text">
				<a href="<c:url value='?lang=cs' />" class="navbar-link">CZ</a> | 
				<a href="<c:url value='?lang=en' />" class="navbar-link">EN</a>
			</p>
			
		</div>

		<div id="navbar" class="navbar-collapse collapse">

			<sec:authorize access="isAuthenticated()">
				<p class="navbar-text navbar-right">
					<a href="<c:url value='/logout' />" class="navbar-link"><spring:message code="logout.link" /> </a>
				</p>
				<p class="navbar-text navbar-right">
					<sec:authentication property="principal.username" />
				</p>
			</sec:authorize>

			<sec:authorize access="!isAuthenticated()">
				<p class="navbar-text navbar-right">
					<a href="<c:url value='/login' />" class="navbar-link"><spring:message code="login.link" /> </a>
				</p>
			</sec:authorize>

		</div>
	</div>
</nav>