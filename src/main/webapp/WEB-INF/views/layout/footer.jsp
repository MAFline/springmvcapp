<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="container">
	<hr>
	<footer>
		<a href="<c:url value='/' />" class="btn btn-default active" role="button">Rozcestník</a>
	</footer>
</div>


<script src="<c:url value='/resources/js/jquery-2.1.1.min.js' />"></script>
<script src="<c:url value='/resources/js/bootstrap.js' />"></script>