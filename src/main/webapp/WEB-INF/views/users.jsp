<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<jsp:include page="layout/header.jsp" />

<body>

	<jsp:include page="layout/navbar.jsp" />

	<div class="container">

		<h1 class="text-center">Uživatel</h1>

		<c:if test="${userFound == false}">
			<div class="alert alert-danger" role="alert">Uživatel
				neexistuje!</div>
		</c:if>

		<c:if test="${userSaved == true}">
			<div class="alert alert-success" role="alert">Uživatel úspěšně
				vytvořen!</div>
		</c:if>

		<c:if test="${userDeleted == true}">
			<div class="alert alert-success" role="alert">Uživatel byl
				úspěšně smazán!</div>
		</c:if>

		<c:if test="${userDeleted == false}">
			<div class="alert alert-danger" role="alert">Smazání uživatele
				se nezdařilo!</div>
		</c:if>


		<a href="<c:url value='/admin/addUser' />"
			class="btn btn-primary active margin-bottom" role="button">Přidat
			uživatele</a>

		<c:if test="${empty users}">
			<div class="alert alert-info" role="alert">Seznam uživatelů je
				prázdný!</div>
		</c:if>

		<c:if test="${!empty users}">

			<div class="table-responsive">
				<table class="table table-hover user-table">

					<thead>
						<tr>
							<th>Jméno</th>
							<th>Upravit</th>
							<th>Smazat</th>
						</tr>
					</thead>

					<tbody>
						<c:forEach var="user" items="${users}">
							<tr>
								<td><a href="<c:url value='/user/${user.id}' />"><c:out
											value="${user.name}" /></a></td>
								<td><a
									href="<c:url value='/admin/editUser/${user.id}' />"
									class="btn btn-success btn-xs">Upravit</a></td>
								<td><a
									href="<c:url value='/admin/deleteUser/${user.id}' />"
									class="btn btn-success btn-xs">Smazat</a></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</c:if>

	</div>

</body>

<jsp:include page="layout/footer.jsp" />

</html>