package cz.xf.masi.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import cz.xf.masi.model.Book;

@Repository
public class BookDaoImpl implements BookDao {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	@Transactional
	public void addBook(Book book) {
		entityManager.persist(book);

	}

	@Override
	@Transactional
	public boolean editBook(Book book) {
		Book dbBook = entityManager.find(Book.class, book.getId());
		if (dbBook == null)
			return false;
		dbBook.setTitle(book.getTitle());
		dbBook.setDescription(book.getDescription());
		return true;
	}

	@Override
	@Transactional
	public boolean deleteBookById(int id) {
		Book dbBook = entityManager.find(Book.class, id);
		if (dbBook == null)
			return false;
		entityManager.remove(dbBook);
		return true;
	}

	@Override
	@Transactional(readOnly = true)
	public Book getBookById(int id) {
		Book book = entityManager.find(Book.class, id);
		return book;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Book> getUserBooks(int userId) {
		List<Book> books = entityManager.createQuery("SELECT b FROM Book b WHERE b.user.id = :userId", Book.class)
				.setParameter("userId", userId).getResultList();
		return books;
	}

}
