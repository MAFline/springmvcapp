package cz.xf.masi.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import cz.xf.masi.model.Account;
import cz.xf.masi.model.Authority;
import cz.xf.masi.model.Book;
import cz.xf.masi.model.User;

@Repository
public class UserDaoImpl implements UserDao {

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private BookDao bookDao;

	@Autowired
	private AccountDao accountDao;

	@Override
	@Transactional(readOnly = true)
	public User getUserById(int id) {
		List<User> users = entityManager
				.createQuery("SELECT u FROM User u LEFT JOIN FETCH u.books WHERE u.id = :id", User.class)
				.setParameter("id", id).getResultList();

		if (!users.isEmpty()) {
			User user = users.get(0);
			user.getAccounts().size();
			user.setPreferableBook();
			user.setPreferableAccount();
			return user;
		}

		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public List<User> getAllUsers() {
		List<User> users = entityManager.createQuery("SELECT u FROM User u ORDER BY u.name", User.class)
				.getResultList();
		return users;
	}

	@Override
	@Transactional
	public void addUser(User user) {
		entityManager.persist(user);
		for (Authority authority : user.getAuthorities()) {
			entityManager.persist(authority);
		}
	}

	@Override
	@Transactional
	public boolean editUser(User user) {
		User dbUser = entityManager.find(User.class, user.getId());
		if (dbUser == null)
			return false;

		// nastaveni preferovane knihy
		List<Book> books = bookDao.getUserBooks(user.getId());
		for (Book book : books) {
			if (book.getId() == user.getPreferableBookId()) {
				book.setIsPreferable(true);
			} else {
				book.setIsPreferable(false);
			}
		}

		// nastaveni preferovaneho uctu
		List<Account> accounts = accountDao.getUserAccounts(user.getId());
		for (Account account : accounts) {
			if (account.getId() == user.getPreferableAccountId()) {
				account.setIsPreferable(true);
			} else {
				account.setIsPreferable(false);
			}
		}

		dbUser.setName(user.getName());
		return true;
	}

	@Override
	@Transactional
	public boolean deleteUserById(int id) {
		User dbUser = entityManager.find(User.class, id);
		if (dbUser == null)
			return false;
		entityManager.remove(dbUser);
		return true;
	}

	@Override
	@Transactional(readOnly = true)
	public long getNumberOfUsers() {
		long numberOfUsers = (long) entityManager.createQuery("SELECT COUNT(u.id) FROM User u").getSingleResult();
		return numberOfUsers;
	}

	@Override
	public boolean isUserNameInDatabase(String name, int userId) {
		int size = entityManager
				.createQuery("SELECT u FROM User u WHERE u.name = :name AND u.id != :userId", User.class)
				.setParameter("name", name).setParameter("userId", userId).getResultList().size();

		if (size != 0)
			return true;
		return false;
	}

}