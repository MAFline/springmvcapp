package cz.xf.masi.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import cz.xf.masi.model.Account;

@Repository
public class AccountDaoImpl implements AccountDao {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	@Transactional(readOnly = true)
	public Account getAccountById(int id) {
		Account account = entityManager.find(Account.class, id);
		return account;
	}

	@Override
	@Transactional
	public void addAccount(Account account) {
		entityManager.persist(account);
	}

	@Override
	@Transactional
	public boolean editAccount(Account account) {
		Account dbAccount = entityManager.find(Account.class, account.getId());
		if (dbAccount == null)
			return false;
		dbAccount.setName(account.getName());
		dbAccount.setAccountPrefix(account.getAccountPrefix());
		dbAccount.setAccountNumber(account.getAccountNumber());
		dbAccount.setBankCode(account.getBankCode());
		return true;
	}

	@Override
	@Transactional
	public boolean deleteAccountById(int id) {
		Account dbAccount = entityManager.find(Account.class, id);
		if (dbAccount == null)
			return false;
		entityManager.remove(dbAccount);
		return true;
	}

	@Override
	public List<Account> getUserAccounts(int userId) {
		List<Account> accounts = entityManager
				.createQuery("SELECT a FROM Account a WHERE a.user.id = :userId", Account.class)
				.setParameter("userId", userId).getResultList();
		return accounts;
	}

}
