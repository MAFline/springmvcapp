package cz.xf.masi.dao;

import java.util.List;

import cz.xf.masi.model.Account;

public interface AccountDao {
	public Account getAccountById(int id);
	public void addAccount(Account account);
	public boolean editAccount(Account account);
	public boolean deleteAccountById(int id);
	public List<Account> getUserAccounts(int userId);
}
