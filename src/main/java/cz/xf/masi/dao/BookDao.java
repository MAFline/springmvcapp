package cz.xf.masi.dao;

import java.util.List;

import cz.xf.masi.model.Book;

public interface BookDao {
	public Book getBookById(int id);
	public void addBook(Book book);
	public boolean editBook(Book book);
	public boolean deleteBookById(int id);
	public List<Book> getUserBooks(int userId);
}
