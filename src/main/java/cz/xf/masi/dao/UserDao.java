package cz.xf.masi.dao;

import java.util.List;

import cz.xf.masi.model.User;

public interface UserDao {
	public User getUserById(int id);
	public List<User> getAllUsers();
	public void addUser(User user);
	public boolean editUser(User user);
	public boolean deleteUserById(int id);
	public long getNumberOfUsers();
	public boolean isUserNameInDatabase(String name, int userId);
}