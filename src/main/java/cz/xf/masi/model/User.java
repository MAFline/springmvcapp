package cz.xf.masi.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Entity
@Table(name = "users")
public class User {
	
	@Id
	@SequenceGenerator(name="seq_users", sequenceName="seq_users", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_users")
	@Column(name="id_user", nullable=false, unique=true)
	private int id;
	
	@Column(name="name", nullable=false, unique=true, length=50)
	@NotBlank(message = "Toto pole je povinné.")
	@NotNull(message = "Toto pole je povinné.")
	private String name;
	
	@Column(name="password", nullable=false, length=100)
	private String password;
	
	@OneToMany(mappedBy = "user", cascade = {CascadeType.REMOVE})
	private List<Book> books;
	
	@OneToMany(mappedBy = "user", cascade = {CascadeType.REMOVE})
	private List<Account> accounts;
	
	@Column(name="enabled", nullable=false)
	private boolean enabled;
	
	@OneToMany(mappedBy = "user", cascade = {CascadeType.REMOVE})
	private List<Authority> authorities;
	
	@Transient
	private Integer preferableBookId;
	
	@Transient
	private Integer preferableAccountId;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public List<Book> getBooks() {
		return books;
	}
	public void setBooks(List<Book> books) {
		this.books = books;
	}
	public List<Account> getAccounts() {
		return accounts;
	}
	public void setAccounts(List<Account> accounts) {
		this.accounts = accounts;
	}
	public List<Authority> getAuthorities() {
		return authorities;
	}
	public void setAuthorities(List<Authority> authorities) {
		this.authorities = authorities;
	}
	public Integer getPreferableBookId() {
		return preferableBookId;
	}
	public void setPreferableBookId(Integer preferableBookId) {
		this.preferableBookId = preferableBookId;
	}
	public boolean getEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	public Integer getPreferableAccountId() {
		return preferableAccountId;
	}
	public void setPreferableAccountId(Integer preferableAccountId) {
		this.preferableAccountId = preferableAccountId;
	}
	
	public void setPreferableBook() {
		for (Book book : books) {
			if (book.getIsPreferable()) {
				preferableBookId = book.getId();
				break;
			}
		}
	}
	
	public void setPreferableAccount() {
		for (Account account : accounts) {
			if (account.getIsPreferable()) {
				preferableAccountId = account.getId();
				break;
			}
		}
	}
	
	public void hasPassword() {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		password = passwordEncoder.encode(password);
	}
	
}