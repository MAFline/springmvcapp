package cz.xf.masi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name = "accounts")
public class Account {
	
	@Id
	@SequenceGenerator(name="seq_accounts", sequenceName="seq_accounts", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_accounts")
	@Column(name="id_account", nullable=false, unique=true)
	private int id;
	
	@Column(name="name", nullable=false, length=50)
	@NotBlank(message = "Toto pole je povinné.")
	@NotNull(message = "Toto pole je povinné.")
	private String name;
	
	@Column(name="account_prefix", nullable=false)
	@NotNull(message = "Toto pole je povinné.")
	@Min(value = 1, message = "Toto pole je povinné.")
	private Integer accountPrefix;
	
	@Column(name="account_number", nullable=false)
	@NotNull(message = "Toto pole je povinné.")
	@Min(value = 1, message = "Toto pole je povinné.")
	private Integer accountNumber;
	
	@Column(name="bank_code", nullable=false)
	@NotNull(message = "Toto pole je povinné.")
	@Min(value = 1, message = "Toto pole je povinné.")
	private Integer bankCode;
	
	@ManyToOne
	@JoinColumn(name = "id_user", nullable = false)
	private User user;
	
	@Column(name="is_preferable", nullable=false)
	@NotNull(message = "Toto pole je povinné.")
	private boolean isPreferable;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAccountPrefix() {
		return accountPrefix;
	}

	public void setAccountPrefix(Integer accountPrefix) {
		this.accountPrefix = accountPrefix;
	}

	public Integer getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Integer getBankCode() {
		return bankCode;
	}

	public void setBankCode(Integer bankCode) {
		this.bankCode = bankCode;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public boolean getIsPreferable() {
		return isPreferable;
	}

	public void setIsPreferable(boolean isPreferable) {
		this.isPreferable = isPreferable;
	}
}
