package cz.xf.masi.model;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Person {
	
	private String name;
	private String surname;
	private int phoneNumber;
	private Date date;
	private List<String> likes;
	private Map<Integer, String> friends;
	
	public Person() {
	}
	
	public static Person generatePerson() {
		Person person = new Person();
		person.setName("Karel");
		person.setSurname("Novák");
		person.setPhoneNumber(777472819);
		person.setDate(new Date());
		person.setLikes(Arrays.asList("fotbal", "deskové hry", "kolo", "pivo"));
		
		Map<Integer, String> friends = new HashMap<Integer, String>();
		friends.put(721097272, "Jirka Dosoudil");
		friends.put(737887998, "Honza Klempíř");
		friends.put(742298222, "Ota Mádle");
		person.setFriends(friends);
		
		return person;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public int getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public List<String> getLikes() {
		return likes;
	}

	public void setLikes(List<String> likes) {
		this.likes = likes;
	}

	public Map<Integer, String> getFriends() {
		return friends;
	}

	public void setFriends(Map<Integer, String> friends) {
		this.friends = friends;
	}
	
}
