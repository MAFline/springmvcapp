package cz.xf.masi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name = "books")
public class Book {
	
	@Id
	@SequenceGenerator(name="seq_books", sequenceName="seq_books", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_books")
	@Column(name="id_book", nullable=false, unique=true)
	private int id;
	
	@Column(name="title", nullable=false, length=50)
	@NotBlank(message = "Toto pole je povinné.")
	@NotNull(message = "Toto pole je povinné.")
	private String title;
	
	@Column(name="description", nullable=false, length=255)
	@NotBlank(message = "Toto pole je povinné.")
	@NotNull(message = "Toto pole je povinné.")
	private String description;
	
	@Column(name="is_preferable", nullable=false)
	@NotNull(message = "Toto pole je povinné.")
	private boolean isPreferable;
	
	@ManyToOne
	@JoinColumn(name = "id_user", nullable = false)
	private User user;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public boolean getIsPreferable() {
		return isPreferable;
	}

	public void setIsPreferable(boolean isPreferable) {
		this.isPreferable = isPreferable;
	}

}
