package cz.xf.masi.interceptors;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import cz.xf.masi.dao.UserDao;

public class InfoInterceptor extends HandlerInterceptorAdapter {
	@Autowired
	private UserDao userDao;

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		if (modelAndView != null && !modelAndView.getViewName().startsWith("redirect:")) {
			ModelMap modelMap = modelAndView.getModelMap();
			modelMap.addAttribute("numberOfUsers", userDao.getNumberOfUsers());
			modelMap.addAttribute("date", new Date());
		}
	}
	
	
	
	
}
