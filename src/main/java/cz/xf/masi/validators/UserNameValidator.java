package cz.xf.masi.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import cz.xf.masi.dao.UserDao;
import cz.xf.masi.model.User;

@Component
public class UserNameValidator implements Validator {
	@Autowired
	private UserDao userDao;

	@Override
	public boolean supports(Class<?> clazz) {
		return User.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		User user = (User) target;
		if (userDao.isUserNameInDatabase(user.getName(), user.getId())) {
			errors.rejectValue("name", "exist", "Toto jméno je již obsazeno.");
		}
	}

}
