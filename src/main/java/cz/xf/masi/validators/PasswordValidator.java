package cz.xf.masi.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import cz.xf.masi.model.User;

@Component
public class PasswordValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return User.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		String password = (String) target;
		if (password == null || password.isEmpty()) {
			errors.rejectValue("password", "empty", "Toto pole je povinné.");
		}
	}

}
