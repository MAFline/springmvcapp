package cz.xf.masi.controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/showPicture")
public class PictureController {
	private @Value("#{config['picture.source']}") String sourceFolder;
	
	@ResponseBody
	@RequestMapping(method = RequestMethod.GET, produces = {
			MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_PNG_VALUE,
			MediaType.IMAGE_GIF_VALUE })
	public byte[] showPicture(@RequestParam("filename") String filename) {
		File file = new File(sourceFolder + filename);
		if (!file.exists()) {
			return null; 
		} 
		
		byte[] picture;
		
		try {
			picture = IOUtils.toByteArray(new FileInputStream(file));
		} catch (IOException e) {
			return null;
		}
		
		return picture;
	}
	
}
