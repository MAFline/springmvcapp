package cz.xf.masi.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import cz.xf.masi.dao.UserDao;
import cz.xf.masi.model.User;

@Controller
public class UserController {
	@Autowired
	private UserDao userDao;
	
	@RequestMapping(value = "/user/{userId}", method = RequestMethod.GET)
	public String getUser(Model model, @PathVariable int userId, RedirectAttributes redirectAttributes) {
		User user = userDao.getUserById(userId);
		if (user == null) {
			redirectAttributes.addFlashAttribute("userFound", false);
			return "redirect:/users";
		}
		model.addAttribute("user", user);
		return "user";
	}

	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public String getUsers(Model model) {
		List<User> users = userDao.getAllUsers();
		model.addAttribute("users", users);
		return "users";
	}
	
}