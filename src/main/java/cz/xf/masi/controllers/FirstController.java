package cz.xf.masi.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cz.xf.masi.model.Person;

@Controller
@RequestMapping("/firstController")
public class FirstController {

	@RequestMapping(method=RequestMethod.GET)
	public String getTestModel(Model model) {
		Person person = Person.generatePerson();
		model.addAttribute("person", person);
		
		return "firstView";
	}

}
