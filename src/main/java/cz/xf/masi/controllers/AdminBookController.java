package cz.xf.masi.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import cz.xf.masi.dao.BookDao;
import cz.xf.masi.dao.UserDao;
import cz.xf.masi.model.Book;
import cz.xf.masi.model.User;

@Controller
@RequestMapping(value = "/admin/")
public class AdminBookController {
	@Autowired
	private UserDao userDao;

	@Autowired
	private BookDao bookDao;

	@RequestMapping(value = "addBook/{userId}", method = RequestMethod.GET)
	public String showAddBookForm(Model model, @PathVariable int userId, RedirectAttributes redirectAttributes) {
		User user = userDao.getUserById(userId);
		if (user == null) {
			redirectAttributes.addFlashAttribute("userFound", false);
			return "redirect:/users";
		}

		model.addAttribute("book", new Book());
		redirectAttributes.addFlashAttribute("userId", userId);
		return "addBook";
	}

	@RequestMapping(value = "addBook/{userId}", method = RequestMethod.POST)
	public String addBook(Model model, @PathVariable int userId, @Valid Book book, BindingResult bindingResult,
			RedirectAttributes redirectAttributes) {
		User user = userDao.getUserById(userId);
		if (user == null) {
			redirectAttributes.addFlashAttribute("userFound", false);
			return "redirect:/users";
		}

		if (bindingResult.hasErrors()) {
			return "addBook";
		}

		book.setUser(user);
		bookDao.addBook(book);
		redirectAttributes.addFlashAttribute("bookAdded", true);
		return "redirect:/user/" + userId;
	}

	@RequestMapping(value = "deleteBook/{userId}/{bookId}", method = RequestMethod.GET)
	public String deleteBook(@PathVariable int userId, @PathVariable int bookId,
			RedirectAttributes redirectAttributes) {
		boolean status = bookDao.deleteBookById(bookId);
		redirectAttributes.addFlashAttribute("bookDeleted", status);
		return "redirect:/user/" + userId;
	}

	@RequestMapping(value = "editBook/{userId}/{bookId}", method = RequestMethod.GET)
	public String showEditBookForm(Model model, @PathVariable int userId, @PathVariable int bookId,
			RedirectAttributes redirectAttributes) {
		Book book = bookDao.getBookById(bookId);
		if (book == null) {
			redirectAttributes.addFlashAttribute("bookFound", false);
			return "redirect:/user/" + userId;
		}

		redirectAttributes.addFlashAttribute("userId", userId);
		model.addAttribute("book", book);
		return "editBook";
	}

	@RequestMapping(value = "editBook/{userId}", method = RequestMethod.POST)
	public String editBook(@PathVariable int userId, @Valid Book book, BindingResult bindingResult,
			RedirectAttributes redirectAttributes) {
		Book originalBook = bookDao.getBookById(book.getId());
		if (originalBook == null) {
			redirectAttributes.addFlashAttribute("bookFound", false);
			return "redirect:/user/" + userId;
		}

		if (bindingResult.hasErrors())
			return "editBook";

		boolean edited = bookDao.editBook(book);
		redirectAttributes.addFlashAttribute("bookEdited", edited);
		return "redirect:/user/" + userId;
	}

}
