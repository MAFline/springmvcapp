package cz.xf.masi.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import cz.xf.masi.dao.AccountDao;
import cz.xf.masi.dao.UserDao;
import cz.xf.masi.model.Account;
import cz.xf.masi.model.User;

@Controller
@RequestMapping(value = "/admin/")
public class AdminAccountController {
	@Autowired
	private UserDao userDao;

	@Autowired
	private AccountDao accountDao;

	@RequestMapping(value = "addAccount/{userId}", method = RequestMethod.GET)
	public String showAddAccountForm(Model model, @PathVariable int userId, RedirectAttributes redirectAttributes) {
		User user = userDao.getUserById(userId);
		if (user == null) {
			redirectAttributes.addFlashAttribute("userFound", false);
			return "redirect:/users";
		}

		model.addAttribute("account", new Account());
		redirectAttributes.addFlashAttribute("userId", userId);
		return "addAccount";
	}

	@RequestMapping(value = "addAccount/{userId}", method = RequestMethod.POST)
	public String addAccount(Model model, @PathVariable int userId, @Valid Account account, BindingResult bindingResult,
			RedirectAttributes redirectAttributes) {
		User user = userDao.getUserById(userId);
		if (user == null) {
			redirectAttributes.addFlashAttribute("userFound", false);
			return "redirect:/users";
		}

		if (bindingResult.hasErrors()) {
			return "addAccount";
		}

		account.setUser(user);
		accountDao.addAccount(account);
		redirectAttributes.addFlashAttribute("accountAdded", true);
		return "redirect:/user/" + userId;
	}

	@RequestMapping(value = "deleteAccount/{userId}/{accountId}", method = RequestMethod.GET)
	public String deleteAccount(@PathVariable int userId, @PathVariable int accountId,
			RedirectAttributes redirectAttributes) {
		boolean status = accountDao.deleteAccountById(accountId);
		redirectAttributes.addFlashAttribute("accountDeleted", status);
		return "redirect:/user/" + userId;
	}

	@RequestMapping(value = "editAccount/{userId}/{accountId}", method = RequestMethod.GET)
	public String showEditAccountForm(Model model, @PathVariable int userId, @PathVariable int accountId,
			RedirectAttributes redirectAttributes) {
		Account account = accountDao.getAccountById(accountId);
		if (account == null) {
			redirectAttributes.addFlashAttribute("accountFound", false);
			return "redirect:/user/" + userId;
		}

		redirectAttributes.addFlashAttribute("userId", userId);
		model.addAttribute("account", account);
		return "editAccount";
	}

	@RequestMapping(value = "editAccount/{userId}", method = RequestMethod.POST)
	public String editAccount(@PathVariable int userId, @Valid Account account, BindingResult bindingResult,
			RedirectAttributes redirectAttributes) {
		Account originalAccount = accountDao.getAccountById(account.getId());
		if (originalAccount == null) {
			redirectAttributes.addFlashAttribute("accountFound", false);
			return "redirect:/user/" + userId;
		}

		if (bindingResult.hasErrors())
			return "editAccount";

		boolean edited = accountDao.editAccount(account);
		redirectAttributes.addFlashAttribute("accountEdited", edited);
		return "redirect:/user/" + userId;
	}

}
