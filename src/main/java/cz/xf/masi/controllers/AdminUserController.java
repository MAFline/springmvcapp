package cz.xf.masi.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import cz.xf.masi.dao.UserDao;
import cz.xf.masi.model.Authority;
import cz.xf.masi.model.User;
import cz.xf.masi.validators.PasswordValidator;
import cz.xf.masi.validators.UserNameValidator;

@Controller
@RequestMapping(value = "/admin/")
public class AdminUserController {
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private UserNameValidator userNameValidator;
	
	@Autowired
	private PasswordValidator passwordValidator;
	
	@RequestMapping(value = "editUser/{userId}", method = RequestMethod.GET)
	public String showEditUserForm(Model model, @PathVariable int userId, RedirectAttributes redirectAttributes) {
		User user = userDao.getUserById(userId);
		if (user == null) {
			redirectAttributes.addFlashAttribute("userFound", false);
			return "redirect:/users";
		}
	
		model.addAttribute("user", user);
		return "editUser";
	}

	@RequestMapping(value = "editUser", method = RequestMethod.POST)
	public String editUser(Model model, @Valid User user, BindingResult bindingResult,
			RedirectAttributes redirectAttributes) {
		User originalUser = userDao.getUserById(user.getId());
		if (originalUser == null) {
			redirectAttributes.addFlashAttribute("userFound", false);
			return "redirect:/users";
		}
		
		//validace jmena
		userNameValidator.validate(user, bindingResult);

		if (bindingResult.hasErrors()) {
			return "editUser";
		}
			

		boolean edited = userDao.editUser(user);
		redirectAttributes.addFlashAttribute("userEdited", edited);
		return "redirect:/user/" + user.getId();
	}

	@RequestMapping(value = "addUser", method = RequestMethod.GET)
	public String showAddUserForm(Model model) {
		model.addAttribute("user", new User());
		return "addUser";
	}

	@RequestMapping(value = "addUser", method = RequestMethod.POST)
	public String addUser(@Valid User user, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
		// validace jmena a hesla
		userNameValidator.validate(user, bindingResult);
		passwordValidator.validate(user.getPassword(), bindingResult);
		
		if (bindingResult.hasErrors()) {
			user.setPassword(null);
			return "addUser";
		}
			

		user.hasPassword();
		user.setEnabled(true);
		Authority authority = new Authority();
		authority.setAuthority("ROLE_USER");
		authority.setUser(user);
		List<Authority> authorities = new ArrayList<>();
		authorities.add(authority);
		user.setAuthorities(authorities);
		
		userDao.addUser(user);
		redirectAttributes.addFlashAttribute("userSaved", true);
		return "redirect:/users";
	}
	
	@RequestMapping(value = "deleteUser/{userId}", method = RequestMethod.GET)
	public String deleteUser(@PathVariable int userId, RedirectAttributes redirectAttributes) {
		boolean status = userDao.deleteUserById(userId);
		redirectAttributes.addFlashAttribute("userDeleted", status);
		return "redirect:/users";
	}
	
}
