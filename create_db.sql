-- create database "morosystems" with owner "morosystems" encoding 'UTF8' lc_collate = 'cs_CZ.UTF-8';

CREATE SEQUENCE seq_users START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE seq_books START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE seq_accounts START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE seq_user_roles START WITH 1 INCREMENT BY 1;

CREATE TABLE users (id_user INTEGER PRIMARY KEY DEFAULT nextval('seq_users'), name VARCHAR(50) UNIQUE NOT NULL, password VARCHAR(100) NOT NULL, enabled BOOLEAN NOT NULL);
CREATE TABLE books (id_book INTEGER PRIMARY KEY DEFAULT nextval('seq_books'), title VARCHAR(50) NOT NULL, description VARCHAR(255) NOT NULL, is_preferable BOOLEAN NOT NULL DEFAULT FALSE, id_user INTEGER NOT NULL REFERENCES users);
CREATE TABLE accounts (id_account INTEGER PRIMARY KEY DEFAULT nextval('seq_accounts'), name VARCHAR(50) NOT NULL, account_prefix INTEGER NOT NULL, account_number INTEGER NOT NULL, bank_code INTEGER NOT NULL, is_preferable BOOLEAN NOT NULL DEFAULT FALSE, id_user INTEGER NOT NULL REFERENCES users);
CREATE TABLE user_roles (id_user_role INTEGER PRIMARY KEY DEFAULT nextval('seq_user_roles'), authority VARCHAR(50) NOT NULL, id_user INTEGER NOT NULL REFERENCES users, UNIQUE (authority, id_user));

-- vlozeni administratora --
-- username: admin
-- password: admin
INSERT INTO users (name, password, enabled) VALUES ('admin', '$2a$10$vfrqg629vuqMh4Z0SCG2fubmOr2sBJCLB.mPTMVf4YJEoYOFwvOCq', TRUE);
INSERT INTO user_roles (authority, id_user) VALUES ('ROLE_ADMIN', currval('seq_users'));
INSERT INTO user_roles (authority, id_user) VALUES ('ROLE_USER', currval('seq_users'));